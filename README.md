Caratteristiche da inserire in una versione avanzata
* boss generati casualmente in maniera interessante
* sistema per sbloccare cose in base a eventi avvenuti in gioco
* eventi stagionali e globali a cui partecipano tutti i giocatori
* gioco di media difficoltà (es. dark souls), in cui è necessario comunque giocare ragionando e concentrandosi