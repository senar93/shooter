﻿using UnityEngine;
using System.Collections;

public class SetLevelManagerScript : MonoBehaviour {

	public GameObject BulletList;
	public GameObject PgList;
	public GameObject NpcList;
	public GameObject EnvironmentObjectList;
	public GameObject PlayerList;

	// Use this for initialization
	void Awake () {
		LevelManagerScript.BulletList = BulletList;
		LevelManagerScript.PgList = PgList;
		LevelManagerScript.NpcList = NpcList;
		LevelManagerScript.EnvironmentObjectList = EnvironmentObjectList;
		LevelManagerScript.PlayerList = PlayerList;

		Destroy(this);
	}
	
	
}
