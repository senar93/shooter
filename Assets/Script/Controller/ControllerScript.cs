﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// esegue le azioni sul pg in base agli input virtuali inseriti
/// </summary>
public class ControllerScript : MonoBehaviour {

	/// <summary>
	/// fonte degli input virtuali
	/// </summary>
	public AbstractInputInterface inputInterface;
	/// <summary>
	/// fonte degli input di rotazione virtuali
	/// </summary>
	public AbstractInputInterfaceRotation inputInterfaceRotation;
	/// <summary>
	/// script che permette all oggetto di muoversi
	/// </summary>
	public PlayerMovementScript movementScript;

	public List<AbstractAbility> abilityList;

	/// <summary>
	/// contiene il numero che rappresenta la layar in cui si trova il pavimento,
	/// deve sempre esserci un oggetto Floor nel livello, con Layer Floor per il corretto funzionamento del gioco
	/// </summary>
	protected int floorMask;
	protected float rayLenght = 10000f;

	public void Awake() {
		floorMask = LayerMask.GetMask("Floor");
	}



	/// <summary>
	/// input non riguardanti la fisica
	/// </summary>
	void Update () {
		if(inputInterface != null) {
			
		}
	}

	/// <summary>
	/// input riguardanti la fisica
	/// </summary>
	void FixedUpdate() {
		if (inputInterface != null) {
			Movement();
			Rotation();
			UseAbility();
		}
	}



	/// <summary>
	/// esegue il movimento del pg in base agli input
	/// richiama in ogni caso Move dallo script PlayerMovementScript
	/// </summary>
	//  una possibile ottimizzazione è richiamare Move sol oquando effettivamente si dovrebbe muovere il personaggio
	//  ho scelto di richiamarla in ogni frame perchè Move se gli si passa 0,0 setta moving a false e basta quindi, è comunque un sistema migliorabile
	void Movement() {
		List<EnumVirtualInput> tmpVI = inputInterface.GetInputList();
		int x = 0, z = 0;

		for (int i = 0; i < tmpVI.Count; i++) {
			switch (tmpVI[i]) {
				case EnumVirtualInput.MOVE_UP:
					//Debug.Log("UP");
					z++;
					break;
				case EnumVirtualInput.MOVE_DOWN:
					//Debug.Log("DOWN");
					z--;
					break;
				case EnumVirtualInput.MOVE_RIGHT:
					//Debug.Log("RIGHT");
					x++;
					break;
				case EnumVirtualInput.MOVE_LEFT:
					//Debug.Log("LEFT");
					x--;
					break;
			}
		}

		movementScript.Move(x, z);

	}

	/// <summary>
	/// ruota l'oggetto in base alla posizione del mouse
	/// idealmente la rotazione dovrebbe essere generale e prendere gli input dagli script RotationInputInterface,
	/// ma il metodo per la rotazione è diverso se si usa il mouse o il joystick, 
	/// per il momento ho implementato solo questa funzione inserendola direttamente qui
	/// </summary>
	void Rotation() {
		//Debug.Log(inputInterfaceRotation.GetCoorToPoint());
		Ray camRay = Camera.main.ScreenPointToRay(inputInterfaceRotation.GetCoorToPoint());
		RaycastHit floorHit;
		if(Physics.Raycast(camRay, out floorHit, rayLenght, floorMask)) {
			Vector3 playerToMouse = floorHit.point - transform.position;
			playerToMouse.y = 0f;

			movementScript.Rotation(playerToMouse);
		}
	}

	void UseAbility() {
		List<EnumVirtualInput> tmpVI = inputInterface.GetInputList();
		for (int i = 0; i < tmpVI.Count; i++) {
			switch (tmpVI[i]) {
				case EnumVirtualInput.USE_ABILITY_0:
					if(abilityList.Count > (int)EnumVirtualInput.USE_ABILITY_0 - (int)EnumVirtualInput.USE_ABILITY_0)
						abilityList[0].Use();
					break;
				case EnumVirtualInput.USE_ABILITY_1:
					if (abilityList.Count > (int)EnumVirtualInput.USE_ABILITY_1 - (int)EnumVirtualInput.USE_ABILITY_0)
						abilityList[1].Use();
					break;
				case EnumVirtualInput.USE_ABILITY_2:
					if (abilityList.Count > (int)EnumVirtualInput.USE_ABILITY_2 - (int)EnumVirtualInput.USE_ABILITY_0)
						abilityList[2].Use();
					break;
				case EnumVirtualInput.USE_ABILITY_3:
					if (abilityList.Count > (int)EnumVirtualInput.USE_ABILITY_3 - (int)EnumVirtualInput.USE_ABILITY_0)
						abilityList[3].Use();
					break;
				case EnumVirtualInput.USE_ABILITY_4:
					if (abilityList.Count > (int)EnumVirtualInput.USE_ABILITY_4 - (int)EnumVirtualInput.USE_ABILITY_0)
						abilityList[4].Use();
					break;
				case EnumVirtualInput.USE_ABILITY_5:
					if (abilityList.Count > (int)EnumVirtualInput.USE_ABILITY_5 - (int)EnumVirtualInput.USE_ABILITY_0)
						abilityList[5].Use();
					break;
			}
		}
	}

}
