﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// legge gli input della tastiera e li converte in un una input virtuale per il ControllerScript
/// </summary>
public class KeyboardScript_InputInterface : AbstractInputInterface {

	public KeyCode[] MOVE_UP = { KeyCode.W, KeyCode.W, KeyCode.W };
	public KeyCode[] MOVE_DOWN = { KeyCode.S, KeyCode.S, KeyCode.S };
	public KeyCode[] MOVE_RIGHT = { KeyCode.D, KeyCode.D, KeyCode.D };
	public KeyCode[] MOVE_LEFT = { KeyCode.A, KeyCode.A, KeyCode.A };

	public KeyCode[] USE_ABILITY_0 = { KeyCode.Space, KeyCode.Space, KeyCode.Space };
	public KeyCode[] USE_ABILITY_1 = { KeyCode.E, KeyCode.E, KeyCode.E };

	protected override void SetInputList() {
		#region CHECK MOVE UP / DOWN
		if (Input.GetKey(MOVE_UP[0]) || Input.GetKey(MOVE_UP[1]) || Input.GetKey(MOVE_UP[2])) {
			InputList.Add(EnumVirtualInput.MOVE_UP);
		} /*else*/ if (Input.GetKey(MOVE_DOWN[0]) || Input.GetKey(MOVE_DOWN[1]) || Input.GetKey(MOVE_DOWN[2])) {
			InputList.Add(EnumVirtualInput.MOVE_DOWN);
		}
		#endregion

		#region CHECK MOVE RIGHT / LEFT
		if (Input.GetKey(MOVE_RIGHT[0]) || Input.GetKey(MOVE_RIGHT[1]) || Input.GetKey(MOVE_RIGHT[2])) {
			InputList.Add(EnumVirtualInput.MOVE_RIGHT);
		} /*else*/ if (Input.GetKey(MOVE_LEFT[0]) || Input.GetKey(MOVE_LEFT[1]) || Input.GetKey(MOVE_LEFT[2])) {
			InputList.Add(EnumVirtualInput.MOVE_LEFT);
		}
		#endregion

		if(Input.GetKey(USE_ABILITY_0[0]) || Input.GetKey(USE_ABILITY_0[1]) || Input.GetKey(USE_ABILITY_0[2])) {
			InputList.Add(EnumVirtualInput.USE_ABILITY_0);
		} else if (Input.GetKey(USE_ABILITY_1[0]) || Input.GetKey(USE_ABILITY_1[1]) || Input.GetKey(USE_ABILITY_1[2])) {
			InputList.Add(EnumVirtualInput.USE_ABILITY_1);
		}
	}

}