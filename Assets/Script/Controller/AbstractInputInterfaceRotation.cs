﻿using UnityEngine;
using System.Collections;

public abstract class AbstractInputInterfaceRotation : MonoBehaviour {
	[SerializeField] protected bool getInputInFixedUpdate = true;
	[SerializeField] protected Vector3 coordToPoint = new Vector3(0f, 0f, 0f);



	/// <summary>
	/// imposta la variabile coordToPoint in base al dispositivo di input selezionato
	/// </summary>
	protected abstract void SetCoordToPoint();



	/// <summary>
	/// restituisce la variabile coordToPoint
	/// </summary>
	/// <returns>indica le coordinate a cui puntare</returns>
	public Vector3 GetCoorToPoint() {
		return coordToPoint;
	}

	void Update(){
		if (!getInputInFixedUpdate)
			SetCoordToPoint();
	}
	void FixedUpdate() {
		if (getInputInFixedUpdate)
			SetCoordToPoint();
	}

}