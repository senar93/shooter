﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// legge gli input da una generica fonte e li converte in input virtuali per ControllerScript
/// eventuali input invalidi sono gestiti qui (es. andare avanti e indietro contemporaneamente)
/// </summary>
public abstract class AbstractInputInterface : MonoBehaviour {
	[SerializeField] protected List<EnumVirtualInput> InputList;
	[SerializeField] protected bool getInputInFixedUpdate = true;



	/// <summary>
	/// imposta la lista degli input virtuali in base agli input reali inseriti
	/// </summary>
	protected abstract void SetInputList();



	/// <summary>
	/// restituisce la lista degli input virtuali
	/// </summary>
	/// <returns>lista degli input virtuali inseriti</returns>
	public List<EnumVirtualInput> GetInputList() {
		return InputList;
	}
	/// <summary>
	/// svuota la lista degli input virtuali
	/// </summary>
	protected void ResetInputList() {
		InputList = new List<EnumVirtualInput>();
	}

	void Update() {
		if (!getInputInFixedUpdate) {
			ResetInputList();
			SetInputList();
		}
	}
	void FixedUpdate() {
		if (getInputInFixedUpdate) {
			ResetInputList();
			SetInputList();
		}
	}

}
