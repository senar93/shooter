﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StandardAttackDamage_AttackDamage : AbstractAttackDamage_ObjectStat {

	[SerializeField] protected float originalAttackDamage;
	[SerializeField] protected float actAttackDamage;
	[SerializeField] protected bool autoSet = true;
	[SerializeField] public bool canBeReset = true;


	void Awake() {
		if (autoSet) {
			actAttackDamage = originalAttackDamage;
		}
	}


	public override float GetActValue() {
		return actAttackDamage;
	}

	public override float GetOriginalValue() {
		return originalAttackDamage;
	}

	public override void InitializeAttack(float newValue, List<object> otherParam = null) {
		if (canBeReset && newValue > AbstractAttackDamage_ObjectStat.MinValue) {
			originalAttackDamage = newValue;
			actAttackDamage = newValue;
		}
	}

}
