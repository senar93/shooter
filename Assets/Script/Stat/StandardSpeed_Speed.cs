﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StandardSpeed_Speed : AbstractSpeed_ObjectStat {
	[SerializeField] protected float maxSpeed;
	/// <summary>
	/// contiene la velocità del oggetto
	/// qualsiasi effetto che modifica temporaneamente speed deve ricordare esattamente la modifica effettuata e reimpostarlo al valore corretto al termine
	/// </summary>
	public float actSpeed;
	[SerializeField] protected bool autoSet = true;
	public bool canBeReset = true;


	void Awake() {
		if (autoSet)
			actSpeed = maxSpeed;
	}

	public override float GetActSpeed() {
		if (actSpeed >= minSpeedAllowed)
			return actSpeed;
		return minSpeedAllowed;
	}
	public override float GetMaxSpeed() {
		return maxSpeed;
	}

	public override void InitializeSpeed(float newValue, List<object> otherParam = null) {
		if (canBeReset && newValue > AbstractSpeed_ObjectStat.minSpeedAllowed) {
			maxSpeed = newValue;
			actSpeed = newValue;
		}
	}

}
