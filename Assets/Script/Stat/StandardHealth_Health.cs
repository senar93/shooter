﻿using UnityEngine;
using System.Collections;
using System;

public class StandardHealth_Health : AbstractHealth_ObjectStat {
	public static float minHealthAllowed = 0f;

	[SerializeField] protected float maxHealth;
	[SerializeField] protected float actHealth;
	[SerializeField] protected bool isDead = false;
	[SerializeField] protected bool autoSet = false;



	void Awake() {
		if (autoSet) {
			actHealth = maxHealth;
		}
	}



	public override float GetActHealth() {
		return actHealth;
	}

	public override float GetMaxHealth() {
		return maxHealth;
	}

	public override bool IsDead() {
		return isDead;
	}


	public override bool SubHealth(float change, bool apply = true) {
		if (IsDead())
			return false;

		float tmpHp = ModHealth(-change);

		if (apply) {
			actHealth = tmpHp;
			if (GetActHealth() <= minHealthAllowed)
				isDead = true;
		}

		return true;

	}

	public override bool AddHealth(float change, bool apply = true) {
		if (IsDead())
			return false;

		float tmpHp = ModHealth(change);
		float tmpMaxHp = GetMaxHealth();

		if (GetActHealth() < tmpMaxHp) {
			if (apply) {
				if (tmpHp <= tmpMaxHp)
					actHealth = tmpHp;
				else
					actHealth = tmpMaxHp;
			}
				
			return true;
		} else {
			return false;
		}

	}

	protected override float ModHealth(float change) {
		return actHealth + change;
	}


}
