﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractSpeed_ObjectStat : AbstractObjectStat {
	/// <summary>
	/// velocità minima consentita
	/// </summary>
	public static float minSpeedAllowed = 0f;

	/// <summary>
	/// restituisce la velocità attuale dell oggetto 
	/// </summary>
	/// <returns>velocità attuale</returns>
	public abstract float GetActSpeed();
	/// <summary>
	/// restituisce la velocità massima dell oggetto 
	/// </summary>
	/// <returns>velocità massima</returns>
	public abstract float GetMaxSpeed();

	/// <summary>
	/// inizializa lo script speed con i valori passati come parametri
	/// </summary>
	/// <param name="newValue">nuovo valore di speed</param>
	/// <param name="otherParam">lista di eventuali altri parametri necessari
	///							 il chiamante deve occuparsi di passare i parametri corretti</param>
	public abstract void InitializeSpeed(float newValue, List<object> otherParam = null);


}
