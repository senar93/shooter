﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractHealth_ObjectStat : AbstractObjectStat {

	/// <summary>
	/// restituisce gli hp attuali dell entità
	/// </summary>
	/// <returns>hp del entità</returns>
	public abstract float GetActHealth();
	/// <summary>
	/// restituisce gli hp massimi dell entità
	/// </summary>
	/// <returns>hp massimi dell entità</returns>
	public abstract float GetMaxHealth();
	/// <summary>
	/// ritorna informazioni sul fatto che l'entità sia viva o morta
	/// </summary>
	/// <returns>TRUE: l'entità è viva,
	///			 FALSE: l'entità è morta</returns>
	public abstract bool IsDead();

	/// <summary>
	/// controlla se è possibile aggiungere "change" hp all entità, ed eventualmente applica la modifica
	/// </summary>
	/// <param name="change">hp da aggiungere all entità</param>
	/// <param name="apply">TRUE: applica i cambiamenti
	///						FALSE: controlla solo se è possibile applicare i cambiamenti</param>
	/// <returns>TRUE: i cambiamenti sono leciti;
	///			 FALSE: i cambiamenti non sono leciti</returns>
	public abstract bool AddHealth(float change, bool apply = true);
	/// <summary>
	/// controlla se è possibile sottrarre "change" hp all entità, ed eventualmente applica la modifica
	/// </summary>
	/// <param name="change">hp da sottrarre all entità</param>
	/// <param name="apply">TRUE: applica i cambiamenti;
	///						FALSE: controlla solo se è possibile applicare i cambiamenti</param>
	/// <returns>TRUE: i cambiamenti sono leciti;
	///			 FALSE: i cambiamenti non sono leciti</returns>
	public abstract bool SubHealth(float change, bool apply = true);

	/// <summary>
	/// restituisce il valore aggiornato degli hp del unità di "change"
	/// </summary>
	/// <param name="change">hp da aggiungere o togliere</param>
	/// <returns>nuovo valore degli hp</returns>
	protected abstract float ModHealth(float change);

}
