﻿using UnityEngine;
using System.Collections;
using System;

public class StandardAttackSpeed_AttackSpeed : AbstractAttackSpeed_ObjectStat
{
	[SerializeField] protected float originalAttackSpeed;
	[SerializeField] protected float actAttackSpeed;
	[SerializeField] protected bool autoSet = false;

	[SerializeField] protected float timer = 0f;


	void Awake() {
		if (autoSet) {
			actAttackSpeed = originalAttackSpeed;
		}
	}


	public override float GetActValue() {
		return actAttackSpeed;
	}

	public override float GetOriginalValue() {
		return originalAttackSpeed;
	}




	public override void ResetTimer(float newValue = 0){
		timer = newValue;
	}
	public override bool TimerFlag() {
		if (timer >= GetActValue())
			return true;
		return false;
	}
	

	void Update() {
		if (!TimerFlag())
			timer += Time.deltaTime;
	}

}
