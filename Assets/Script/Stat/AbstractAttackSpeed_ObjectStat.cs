﻿using UnityEngine;
using System.Collections;

public abstract class AbstractAttackSpeed_ObjectStat : AbstractObjectStat {

	static float MinValue = 0f;

	public abstract float GetOriginalValue();
	public abstract float GetActValue();

	public abstract void ResetTimer(float newValue = 0);
	public abstract bool TimerFlag();
}
