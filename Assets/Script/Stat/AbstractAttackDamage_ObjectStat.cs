﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractAttackDamage_ObjectStat : AbstractObjectStat {

	public static float MinValue = 0f;	

	public abstract float GetOriginalValue();
	public abstract float GetActValue();
	/// <summary>
	/// inizializa lo script attack damage con i valori passati come parametri
	/// </summary>
	/// <param name="newValue">nuovo valore di damage</param>
	/// <param name="otherParam">lista di eventuali altri parametri necessari
	///							 il chiamante deve occuparsi di passare i parametri corretti</param>
	public abstract void InitializeAttack(float newValue, List<object> otherParam = null);
	/*public abstract void RawAddAttackDamage(float value);
	public abstract void RawSubAttackDamage(float value);*/

}
