﻿using UnityEngine;
using System.Collections;

public abstract class AbstractAttack : MonoBehaviour {

	public abstract bool CanAttack();
	protected abstract void RawAttack();

	public bool Attack() {
		bool tmpFlag = CanAttack();
		if (tmpFlag)
			RawAttack();
		return tmpFlag;
	}

}
