﻿using UnityEngine;
using System.Collections;
using System;

public class MeleeSingleTargetScript_Attack : AbstractAttack {

	public GameObject target;
	public TEST_TriggerFlagScript range;
	public AbstractAttackDamage_ObjectStat damage;
	public AbstractAttackSpeed_ObjectStat attackSpeed;
	public bool attackEnable = true;

	public override bool CanAttack() {
		if (range.inRange && attackSpeed.TimerFlag() && target != null && attackEnable)
			return true;
		return false;
	}

	protected override void RawAttack() {
		if (CanAttack()) {
			AbstractHealth_ObjectStat tmpObj = target.GetComponent<AbstractHealth_ObjectStat>();
			if (tmpObj != null) {
				tmpObj.SubHealth(damage.GetActValue(), true);
				attackSpeed.ResetTimer();
			}
		}
	}


}
