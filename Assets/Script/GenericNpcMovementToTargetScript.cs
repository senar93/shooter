﻿using UnityEngine;
using System.Collections;

public class GenericNpcMovementToTargetScript : MonoBehaviour {
	public bool canMove = true;
	public bool moving = false;
	public AbstractSpeed_ObjectStat speed;
	/// <summary>
	/// target verso il quale si dirige l'npc
	/// </summary>
	public Transform target;


	[SerializeField] protected NavMeshAgent nav;

	/*void Awake() {
		nav = GetComponent<NavMeshAgent>();
	}*/



	public void Move() {
		if (nav != null) {
			if (canMove && target != null) {
				nav.enabled = true;
				nav.speed = speed.GetActSpeed();
				nav.SetDestination(target.position);
			}
			else {
				nav.enabled = false;
			}
		}
	}



	public void Update() {
		// DA SOSTITUIRE
		// per qualche motivo la y del oggetto se non settata scende a 0,9...
		// quest istruzione sistema il problema, ma va individuata la causa e sistemato in qualche modo più corretto
		transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
	}

}
