﻿using UnityEngine;
using System.Collections;

public class TEST_IaScript : MonoBehaviour {
	
	// da generalizare con una calsse astratta
	public GenericNpcMovementToTargetScript genericMovementScript;
	public AbstractAttack genericAttackScript;

	void FixedUpdate() {
		if (genericAttackScript != null && genericAttackScript.CanAttack())
			genericAttackScript.Attack();
		else if (genericMovementScript != null)
			genericMovementScript.Move();
		
	}

}
