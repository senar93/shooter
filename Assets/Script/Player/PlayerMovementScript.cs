﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour {

	public bool canMove = true;
	public bool canRotate = true;
	public bool moving = false;
	public AbstractSpeed_ObjectStat speed;
	[SerializeField] protected Rigidbody rigidBody;

	/// <summary>
	/// muove il personaggio sugli assi X,Z a velocità speed durante il frame in cui viene richiamata
	/// quando richiamata imposta moving al valore corretto
	/// </summary>
	/// <param name="x">asse X</param>
	/// <param name="z">asse Z</param>
	public void Move(float x, float z) {
		if(canMove)
			if (x == 0 && z == 0)
				moving = false;
			else {
				moving = true;
				Vector3 movement = new Vector3(x, 0f, z);
				movement = movement.normalized * speed.GetActSpeed() * Time.deltaTime;
				rigidBody.MovePosition(rigidBody.transform.position + movement);
			}	
	}
	/// <summary>
	/// muove il personaggio sugli assi X,Z a velocità speed durante il frame in cui viene richiamata
	/// quando richiamata imposta moving al valore corretto
	/// </summary>
	/// <param name="axes">assi, la componente Y viene ignorata</param>
	public void Move(Vector3 axes) {
		Move(axes.x, axes.z);
	}

	/// <summary>
	/// Ruota il personaggio in modo che punti alla posizione X,Z
	/// </summary>
	/// <param name="position">posizione al quale rivolgersi</param>
	public void Rotation(Vector3 position) {
		if (canRotate) {
			Quaternion newRotation = Quaternion.LookRotation(position);
			rigidBody.MoveRotation(newRotation);
		}
	}
	/// <summary>
	/// Ruota il personaggio in modo che punti alla posizione X,Z
	/// </summary>
	/// <param name="x">X della posizione al quale rivolgersi</param>
	/// <param name="z">Z della posizione al quale rivolgersi</param>
	public void Rotation(float x, float z) {
		Rotation(new Vector3(x, 0f, z));
	}

}
