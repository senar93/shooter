﻿using UnityEngine;
using System.Collections;

public class TEST_script : MonoBehaviour {

	public AbstractHealth_ObjectStat hp;

	// Use this for initialization
	void Start () {
		//TestHp();
	}




	void TestHp() {
		Debug.Log("max hp : " + hp.GetMaxHealth());
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("is dead? : " + hp.IsDead());

		Debug.Log("heal 3000 : " + hp.AddHealth(3000f, false));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("heal 3000, apply : " + hp.AddHealth(3000f, true));
		Debug.Log("act hp: " + hp.GetActHealth());

		Debug.Log("damage 30 : " + hp.SubHealth(3000f, false));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("damage 30, apply : " + hp.SubHealth(30f, true));
		Debug.Log("act hp: " + hp.GetActHealth());

		Debug.Log("heal 3000 : " + hp.AddHealth(3000f, false));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("heal 3000, apply : " + hp.AddHealth(3000f, true));
		Debug.Log("act hp: " + hp.GetActHealth());

		Debug.Log("damage 3000 : " + hp.SubHealth(3000f, false));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("is dead? : " + hp.IsDead());
		Debug.Log("damage 3000, apply : " + hp.SubHealth(3000f, true));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("is dead? : " + hp.IsDead());

		Debug.Log("heal 3000, apply : " + hp.AddHealth(3000f, true));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("is dead? : " + hp.IsDead());
		Debug.Log("damage 3000, apply : " + hp.SubHealth(3000f, true));
		Debug.Log("act hp: " + hp.GetActHealth());
		Debug.Log("is dead? : " + hp.IsDead());
	}


}
