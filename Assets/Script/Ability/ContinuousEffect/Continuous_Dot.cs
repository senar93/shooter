﻿using UnityEngine;
using System.Collections;
using System;

public class Continuous_Dot : AbstractDot_ContinousEffect {

	protected override void ApplyEffect() {
		if(AllFine)
			tmpHp.SubHealth(damage.GetActValue() * Time.deltaTime);
	}

}
