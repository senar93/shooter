﻿using UnityEngine;
using System.Collections;

public abstract class AbstractContinuousEffect : MonoBehaviour {

	public bool haveLength = true;
	public float length = 0f;
	/// <summary>
	/// contiene il tempo passato da quando il debuf è stato applicato, se >= length e haveLength = true il debuf scade
	/// </summary>
	[SerializeField] protected float effectTimer = 0f;



	/// <summary>
	/// richiamata all applicazione del debuf
	/// </summary>
	protected abstract void OnStart();
	/// <summary>
	/// funzione richiamata allo scadere del effetto;
	/// le sue implementazioni DEVONO occuparsi della distruzione del oggetto debuf:
	/// in genere Destroy(this.gameObject);
	/// </summary>
	protected abstract void OnEnd();
	protected abstract void ApplyEffect();
	/// <summary>
	/// se implementata permette a script esterni di terminare l'effetto
	/// </summary>
	public abstract void EndEffect();
	/// <summary>
	/// si occupa di settare i parametri del oggetto prima di chiamare OnStart, in modo che l'oggetto possa funzioanre in maniera corretta
	/// </summary>
	/// <param name="target">target del effetto</param>
	public abstract void Initzialize(GameObject target);


	/// <summary>
	/// aggiorna effectTimer quando richiamata
	/// </summary>
	protected void UpdateTimer(){
		if (haveLength){
			if (effectTimer < length)
				effectTimer += Time.deltaTime;
			else
				OnEnd();
		}
	}



	protected virtual void Start() {
		OnStart();
	}

	protected void Update() {
		ApplyEffect();
		UpdateTimer();
	}
	

}
