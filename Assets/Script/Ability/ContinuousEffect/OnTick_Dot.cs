﻿using UnityEngine;
using System.Collections;

public class OnTick_Dot : AbstractDot_ContinousEffect {

	public float tickFrequence = 0f;
	public float tickTimer = 0f;

	protected override void ApplyEffect() {
		if (AllFine) {
			if (tickTimer >= tickFrequence) {
				tmpHp.SubHealth(damage.GetActValue());
				tickTimer = 0f;
			} else
				tickTimer += Time.deltaTime;
		}
	}


}
