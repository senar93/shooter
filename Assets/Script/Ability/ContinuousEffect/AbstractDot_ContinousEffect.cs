﻿using UnityEngine;
using System.Collections;

public abstract class AbstractDot_ContinousEffect : AbstractContinuousEffect {

	public AbstractAttackDamage_ObjectStat damage;
	[SerializeField] protected GameObject target;
	[SerializeField] protected AbstractHealth_ObjectStat tmpHp;
	/// <summary>
	/// se true i vari controlli sono andati a buon fine, e tutto può essere eseguito correttamente
	/// </summary>
	[SerializeField] protected bool AllFine = false;




	public override void Initzialize(GameObject target) {
		this.target = target;
		if (target != null) {
			tmpHp = target.GetComponent<AbstractHealth_ObjectStat>();
			if (tmpHp != null)
				AllFine = true;
		}

		OnStart();
	}



	/// <summary>
	/// per questo oggetto non fa nulla
	/// </summary>
	protected override void OnStart() { }

	protected override void OnEnd() {
		Destroy(this.gameObject);
	}

	public override void EndEffect() {
		OnEnd();
	}




	// override di Start, per rimuovere l'esecuzione automatica di OnStart()
	// OnStart viene eseguito dalla funzione Initzialize(target) che deve essere SEMPRE richiamata alla creazione di quest oggetto
	// --> da commentare se si intende assegnare il target a mano per i test, senza usare Initzialize() <--
	//protected override void Start() { }


}
