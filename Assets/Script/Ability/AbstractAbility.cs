﻿using UnityEngine;
using System.Collections;

public abstract class AbstractAbility : MonoBehaviour {

	public abstract void Use();

}
