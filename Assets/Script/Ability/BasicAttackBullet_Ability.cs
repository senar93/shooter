﻿using UnityEngine;
using System.Collections;
using System;

public class BasicAttackBullet_Ability : AbstractAbility {

	public GameObject bulletPrefab;
	public AbstractAttackDamage_ObjectStat damage;
	public AbstractAttackSpeed_ObjectStat attackSpeed;
	public GameObject spawnPosition;
	public bool CanUse = true;


	public override void Use() {
		if(CanUse) {
			if (attackSpeed.TimerFlag()) {
				GameObject tmpPrefabe = Instantiate(bulletPrefab);
				tmpPrefabe.transform.position = spawnPosition.transform.position;
				tmpPrefabe.GetComponent<ONLY_TEST_BulletScript>().Initzialize(this.transform.rotation.eulerAngles.y);   // da generalizare con un proiettile qualsiasi
				AbstractAttackDamage_ObjectStat tmpAD = tmpPrefabe.GetComponent<AbstractAttackDamage_ObjectStat>();
				if (tmpAD != null)
					tmpAD.InitializeAttack(damage.GetActValue());
				if (LevelManagerScript.BulletList != null)
					tmpPrefabe.transform.parent = LevelManagerScript.BulletList.transform;

				attackSpeed.ResetTimer();
			}
		}
	}


	protected bool CheckValidate() {
		if (bulletPrefab != null && attackSpeed != null && spawnPosition != null)
			return true;
		return false;
	}

	void Update() {
		if (!CheckValidate())
			CanUse = false;
	}


}
