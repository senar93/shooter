﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// FUNZIONA SOLO SU UN SOLO TARGET, colpire più target non da problemi ma SOLO IL PRIMO VIENE CONSIDERATO
/// script di un proiettile standard;
/// se non viene distrutto quando danneggia un bersaglio, applica i suoi danni a tutti i bersagli colpiti ogni fps finche non viene distrutto o non smettono di collidere con lui
/// </summary>
public class ONLY_TEST_BulletScript : MonoBehaviour {
	[System.Serializable] public struct EffectApply {
		public EnumTag tag;
		public GameObject effectPrefab;
	}

	public bool canMove = true;
	public AbstractSpeed_ObjectStat speed;
	public AbstractAttackDamage_ObjectStat damage;
	public TriggerEventScript triggerCollider;
	/// <summary>
	/// direzione in gradi;
	/// da -180 a 180 --> / (180 / Mathf.PI) --> -1 a 1
	/// </summary>
	public float direction;
	[SerializeField] protected float x, z;
	public EnumTag[] destroyWhenHit;
	public EnumTag[] dealDamageWhenHit;
	public EffectApply[] applyEffectWhenHit;

	public void Initzialize(float direction, bool canMove = true) {
		this.direction = direction;
		float directionRadians = direction / (180 / Mathf.PI);
		z = Mathf.Cos(directionRadians);
		x = Mathf.Sin(directionRadians);
		this.transform.rotation = Quaternion.Euler(90f, direction, 0);
		//this.transform.LookAt(new Vector3(0f, direction, 0f));
		this.canMove = canMove;
	}


	/// <summary>
	/// muove l'oggetto
	/// </summary>
	public void Move() {
		if(canMove) {
			Vector3 movement = new Vector3(x, 0f, z);
			movement = movement.normalized * speed.GetActSpeed() * Time.deltaTime;
			//rigidBody.MovePosition(rigidBody.transform.position + movement);
			this.transform.position = this.transform.position + movement;
		}
	}

	/// <summary>
	/// ad ogni fps danneggia il primo oggetto colpito se collide con un elemento con tag contenuto in dealDamageWhenIt
	/// </summary>
	protected void DealDamage() {
		for (int i = 0; i < dealDamageWhenHit.Length; i++) {
			List<GameObject> tmpObj = triggerCollider.CollideWithSomething(dealDamageWhenHit[i]);
			if (tmpObj.Count > 0) {
				AbstractHealth_ObjectStat tmpHp = tmpObj[0].GetComponent<AbstractHealth_ObjectStat>();
				if (tmpHp != null)
					tmpHp.SubHealth(damage.GetActValue());
			}
		}
	}

	/// <summary>
	/// distrugge l'oggetto se collide con un elemento con tag contenuto in destroyWhenIt
	/// </summary>
	protected void SelfDestroy() {
		for (int i = 0; i < destroyWhenHit.Length; i++) {
			if (triggerCollider.CollideWithSomething(destroyWhenHit[i]).Count > 0) {
				Destroy(this.gameObject);
			}
		}
	}

	/// <summary>
	/// spawna una copia del prefab del effetto selezionato come figlio del target
	/// la funzione Initzialize(target) del oggetto va richiamata qui
	/// </summary>
	protected void ApplyEffect() {
		for (int i = 0; i < applyEffectWhenHit.Length && applyEffectWhenHit[i].effectPrefab != null; i++) {
			List<GameObject> tmpObj = triggerCollider.CollideWithSomething(applyEffectWhenHit[i].tag);
			if (tmpObj.Count > 0) {
				GameObject tmpPrefabe = Instantiate(applyEffectWhenHit[i].effectPrefab);
				tmpPrefabe.transform.position = tmpObj[0].transform.position;
				tmpPrefabe.GetComponent<AbstractContinuousEffect>().Initzialize(tmpObj[0]);
				tmpPrefabe.transform.parent = tmpObj[0].transform;
			}
		}
	}

	
	void FixedUpdate() {
		Move();
		if(damage != null)
			DealDamage();
		ApplyEffect();
		SelfDestroy();
	}

	/*void Awake() {
		Initzialize(90);
	}*/


}


/*public struct EffectApply {
	EnumTag tag;
	AbstractContinuousEffect effect;
}*/