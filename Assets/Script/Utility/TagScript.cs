﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TagScript : MonoBehaviour {

	public List<EnumTag> tagList;



	/// <summary>
	/// controlla se il tag passato come parametro è contenuto nella lista dei tag del oggetto
	/// </summary>
	/// <param name="tag">tag da cercare</param>
	/// <returns>TRUE: tag trovato;
	///			 FALSE: tag non trovato</returns>
	public bool FindTag(EnumTag tag) {
		for (int i = 0; i < tagList.Count; i++)
			if (tagList[i] == tag)
				return true;

		return false;
	}

	/// <summary>
	/// cerca se almeno 1 tag nella lista passata come parametro è contenuto nella lista dei tag del oggetto
	/// </summary>
	/// <param name="tags">lista dei tag da cercare</param>
	/// <returns>TRUE: tag trovato;
	///			 FALSE: tag non trovato</returns>
	public bool FindTag(List<EnumTag> tags) {
		for (int i = 0; i < tags.Count; i++)
			if (FindTag(tags[i]))
				return true;

		return false;
	}

	/// <summary>
	/// cerca se almeno 1 tag nella array passato come parametro è contenuto nella lista dei tag del oggetto
	/// </summary>
	/// <param name="tags">lista dei tag da cercare</param>
	/// <returns>TRUE: tag trovato;
	///			 FALSE: tag non trovato</returns>
	public bool FindTag(EnumTag[] tags) {
		return FindTag(new List<EnumTag>(tags));
	}


}
