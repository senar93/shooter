﻿using UnityEngine;
using System.Collections;

public class TEST_DestroyIfDeadScript : MonoBehaviour {

	protected AbstractHealth_ObjectStat hp;

	void Awake() {
		hp = this.GetComponent<AbstractHealth_ObjectStat>();
	}

	// Update is called once per frame
	void Update () {
		if (hp.IsDead())
			Destroy(this.gameObject);
	}
}
