﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerEventScript : MonoBehaviour {

	public List<EnumTag> checkTag;
	public List<GameObject> collideWith;


	/// <summary>
	/// richiamata quando un oggetto entra nel collider di questo oggetto
	/// </summary>
	/// <param name="other">oggetto che fa scattare la chiamata del metodo</param>
	void OnTriggerEnter(Collider other) {
		if (TagFind(other.gameObject)) {
			collideWith.Add(other.gameObject);
		}
	}

	/// <summary>
	/// richiamata quando un oggetto esce dal collider di questo oggetto
	/// </summary>
	/// <param name="other">oggetto che fa scattare la chiamata del metodo</param>
	void OnTriggerExit(Collider other) {
		collideWith.Remove(other.gameObject);
	}


	/// <summary>
	/// resetta la lista di oggetti che collidono con questo
	/// </summary>
	public void ResetCollide() {
		collideWith = new List<GameObject>();
	}

	/// <summary>
	/// controlla se l'oggetto passato come parametro ha almeno uno dei tag elencati nella variabile checkTag
	/// </summary>
	/// <param name="obj">oggetto da controllare</param>
	/// <returns>TRUE: tag trovato, FALSE: tag non trovato</returns>
	public bool TagFind(GameObject obj) {
		TagScript tmpTag = obj.GetComponent<TagScript>();
		if(tmpTag != null)
			return tmpTag.FindTag(checkTag);

		return false;
	}

	/// <summary>
	/// controlla se l'oggetto collide con qualcosa
	/// </summary>
	/// <returns>TRUE: collide con qualcosa, FALSE: non collide con nulla</returns>
	public bool CollideEmpty() {
		if (collideWith.Count > 0)
			return false;

		return true;
	}

	/// <summary>
	/// controlla se c'è uno o più oggetto con un determinato tag che collidono con questo, se si li restituisce al chiamante
	/// </summary>
	/// <param name="tag">tag da controllare</param>
	/// <returns>lista degli oggetti trovati</returns>
	public List<GameObject> CollideWithSomething(EnumTag tag) {
		List<GameObject> tmpList = new List<GameObject>();
		for (int i = 0; i < collideWith.Count; i++) {
			TagScript tmpTag = collideWith[i].GetComponent<TagScript>();
			if (tmpTag != null && tmpTag.FindTag(tag))
				tmpList.Add(collideWith[i]);
				
		}

		return tmpList;
	}

}
