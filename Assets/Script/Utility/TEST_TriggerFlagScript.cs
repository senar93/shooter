﻿using UnityEngine;
using System.Collections;

// utilizabile solo per oggetti di cui è presente un unica copia nella mappa (es. il giocatore, se in single player)
// l'utilizzo in altri casi può causare situazioni non corrette
// (es. essere a raggio di 2 player, se un player esce dal raggio perde le informazioni sul fatto che si trova un altro player nel raggio) 
public class TEST_TriggerFlagScript : MonoBehaviour {

	public bool inRange = false;
	public EnumTag tagToCheck = EnumTag.player;
	

	void OnTriggerEnter(Collider other) {
		TagScript tmpCheck = other.gameObject.GetComponent<TagScript>();
		if(tmpCheck != null && tmpCheck.FindTag(tagToCheck)) {
			inRange = true;
		}
	}

	void OnTriggerExit(Collider other) {
		TagScript tmpCheck = other.gameObject.GetComponent<TagScript>();
		if (tmpCheck != null && tmpCheck.FindTag(tagToCheck)) {
			inRange = false;
		}
	}

}
